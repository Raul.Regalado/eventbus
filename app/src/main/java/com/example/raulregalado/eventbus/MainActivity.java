package com.example.raulregalado.eventbus;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MainActivity extends Activity {

/*
Eventbus 3 simple steps:
    1) Add implementation 'org.greenrobot:eventbus:3.1.1' in gradle
    2) Define a event, a simple POJO is fine.
    3) Post events and prepare subscribers

    http://greenrobot.org/eventbus/documentation/how-to-get-started/
*/

    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Register broadcast receiver
        registerReceiver(new PowerConnectionReceiver(), new IntentFilter(Intent.ACTION_POWER_CONNECTED));
        registerReceiver(new PowerConnectionReceiver(), new IntentFilter(Intent.ACTION_POWER_DISCONNECTED));

        tv = (TextView) findViewById(R.id.tv);
        tv.setText("Waiting for events...");

        // EventBus is a singleton, thus, calling getDefault will bring the unique bus object
        EventBus.getDefault().register(this);

    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    // this method will receive events, therefore, we need to describe what kind of events
    @Subscribe
    public void onEvent(PowerConnectionEvent event){
        tv = (TextView) findViewById(R.id.tv);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append(tv.getText())
                .append("\n")
                .append(event.getMgs())
                .append(" ")
                .append(event.getId());



        Log.d("EVENT FIRED",stringBuilder.toString());
        tv.setText(stringBuilder.toString());
    }
}
