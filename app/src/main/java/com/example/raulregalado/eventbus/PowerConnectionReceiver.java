package com.example.raulregalado.eventbus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.greenrobot.eventbus.EventBus;

import java.util.Date;

public class PowerConnectionReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        PowerConnectionEvent event = null;
        Date date = new Date();
        String eventTime = date.toString();
        String eventData = "@ " + eventTime + " device: ";

        if(intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)){
            event = new PowerConnectionEvent(eventData + "charging",1);
        }else if (intent.getAction().equals(Intent.ACTION_POWER_DISCONNECTED)){
            event = new PowerConnectionEvent(eventData + "NOT charging",-1);
        }

        EventBus.getDefault().post(event);
    }
}
