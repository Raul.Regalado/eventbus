package com.example.raulregalado.eventbus;

class PowerConnectionEvent {
    private String mgs;
    private int id;

    public PowerConnectionEvent(String mgs, int id) {
        this.mgs = mgs;
        this.id = id;
    }

    public String getMgs() {
        return mgs;
    }

    public void setMgs(String mgs) {
        this.mgs = mgs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
